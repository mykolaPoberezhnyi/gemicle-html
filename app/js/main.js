$(function() {
    $('body').on('click', '.to-top', function(event) {
        event.preventDefault();
        $('body,html').animate({ scrollTop: 0 }, 500);
    });
    $('body').on('click', '.menu-toggle', function(event) {
        event.preventDefault();
        $('.menu-list').toggleClass('toggle');
    });
})